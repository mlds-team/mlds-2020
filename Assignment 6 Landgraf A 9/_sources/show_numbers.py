def show_numbers(X, num_samples=90, random=True):
    if random:
      indices = np.random.choice(range(len(X)), num_samples)
    else: 
      indices = range(min([num_samples, len(X)]))
      
    sample_digits = X[indices]

    fig = plt.figure(figsize=(20, 6))

    for i in range(num_samples):
        ax = plt.subplot(6, 15, i + 1)
        img = 1-sample_digits[i].reshape((16, 16))
        plt.imshow(img, cmap='gray')
        plt.axis('off')