import pandas as pd
import numpy as np
import seaborn as sns

from sklearn.decomposition import PCA
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

df = pd.read_csv('iris.csv')
df_setosa = df.copy()
df_setosa['class'] = df_setosa['class'].apply(lambda c: 1 if c == 'Iris-setosa' else 0)
df_setosa_mini = df_setosa[['sepal_length', 'sepal_width', 'class']].copy()
P = df_setosa_mini[df_setosa_mini['class'] == 1]
N = df_setosa_mini[df_setosa_mini['class'] == 0]
p_c = np.average(P, axis=0)
n_c = np.average(N, axis=0)

df_setosa_mini_centered = df_setosa_mini - np.average(np.vstack([p_c, n_c]), axis=0) + np.array([0, 0, 0.5])

y = df_setosa_mini_centered['class']
X = df_setosa_mini_centered.loc[:, df_setosa_mini_centered.columns != 'class']

X = X[['sepal_length', 'sepal_width']]

def random_sample(arr, size=1):
    return arr[np.random.choice(len(arr), size=size, replace=False)]


class Perzeptron:
    def __init__(self, theta=1):
        self.theta = theta
        self.w = None

    def fit(self, X, y):
        Xy = self._merge(X, y)
        self.w = self._get_w(Xy)

    def predict(self, X):
        return [self._activate(x) for x in X.to_numpy()]

    def _merge(self, X, y):
        assert sorted(y.unique().tolist()) == [0, 1]
        return pd.concat([X, y], axis=1).to_numpy()

    def _get_w(self, Xy):
        dist = np.inf
        P = Xy[((Xy[:, 2] > 0))]
        w_ = np.average(P, axis=0)[:-1]
        w = w_
        while dist > self.theta:
            w = w_
            print(w_)
            v_ = random_sample(Xy).reshape(3, )
            v = v_[:-1]
            v_is_p = v_[-1]
            dot = np.dot(w, v)

            if v_is_p and dot > 0:
                continue
            elif v_is_p and dot <= 0:
                w_ = w + v
            elif not v_is_p and dot < 0:
                continue
            else:
                w_ = w - v

            dist = np.linalg.norm(w_ - w)
        return w

    def _activate(self, x):
        return int(np.dot(self.w, x) > 0)


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

clf = Perzeptron()
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)

print(accuracy_score(y_test, y_pred))