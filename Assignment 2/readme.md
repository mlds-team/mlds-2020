# Readme

## Report
The report is uploaded onto the git as "Report Assignment 2.ipynb"

## Exercises
You can find the solution for Assignment 2 in:
https://colab.research.google.com/drive/1jUgrRtoOrTCTuy6-vy_UWZTGa6ecp_fm?usp=sharing

Or locally in the Jupyter Notebook "Assignment 2.ipynb"


## Editor Links
Editor link to the Overleaf project:
https://www.overleaf.com/9433591466vszcwvsztrxz